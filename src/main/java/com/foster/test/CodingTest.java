package com.foster.test;

import com.foster.test.Model.AgeRating;
import com.foster.test.Processors.ReaderParser;
import com.foster.test.Processors.WriterParser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

public class CodingTest {
    public static void main(String[] args){
        System.out.println("Started Processing Asset Data");
        File jsonFile = new File(String.join(File.separator, Paths.get("").toAbsolutePath().toString(), "assets.json"));

        try {
            ReaderParser readParser = new ReaderParser(jsonFile);
            readParser.processFile();
            readParser.close();
            Map<String, AgeRating> result = readParser.getResultMap();

            File resultFile = new File(String.join(File.separator, Paths.get("").toAbsolutePath().toString(), "result.json"));
            if (!resultFile.exists()){
                resultFile.createNewFile();
            }
            WriterParser  writerParser = new WriterParser(resultFile);
            writerParser.writeToJson(result);
            writerParser.flushAndCloseWriter();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Finished Processing Asset Data");
    }
}
