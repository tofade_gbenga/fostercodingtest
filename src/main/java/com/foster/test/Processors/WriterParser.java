package com.foster.test.Processors;

import com.foster.test.Model.AgeRating;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class WriterParser {
    private Gson gsonBuilder;
    private FileWriter fileWriter;

    /**
     * Constructor to instantiate the Json stream parse to the result file specified
     * @param resultFile
     */
    public WriterParser(File resultFile) throws IOException {
        this.gsonBuilder = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        this.fileWriter = new FileWriter(resultFile);
    }

    /**
     * Writes Json to file
     * @param ratings
     * @throws IOException
     */
    public void writeToJson(Map<String, AgeRating> ratings) throws IOException {
        this.gsonBuilder.toJson(ratings, fileWriter);
    }

    /**
     * Flushes and cleans the fileWriter
     */
    public void flushAndCloseWriter(){
        try {
            this.fileWriter.flush();
            this.fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
