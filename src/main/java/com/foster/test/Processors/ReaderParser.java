package com.foster.test.Processors;

import com.foster.test.Model.AgeRating;
import com.foster.test.Model.Movie;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReaderParser {
    private JsonReader jsonReader;
    private Map<String, AgeRating> resultMap = new HashMap<>();

    /**
     * A constructor that accepts a file and instantiates a JSON Reader
     * @param file
     * @throws FileNotFoundException
     */
    public ReaderParser(File file) throws FileNotFoundException {
        this.jsonReader = new JsonReader(new FileReader(file));
    }

    /**
     * Process the reader and build result
     * @throws IOException
     */
    public void processFile() throws IOException {
        this.jsonReader.beginArray();
        while (this.jsonReader.hasNext()){
            JsonToken assetToken = this.jsonReader.peek();

            if (assetToken.equals(JsonToken.BEGIN_OBJECT))
                    buildMovieAsset(this.jsonReader);
        }
        this.jsonReader.endArray();
    }

    /**
     * Build the movie asset from the current reader object
     * @param jsonReader
     */
    private void buildMovieAsset(JsonReader jsonReader) {
        try {
            jsonReader.beginObject();
            Movie movie = new Movie();
            Boolean isMovie = Boolean.FALSE;
            List<String> fskList = new ArrayList<>();

            while (jsonReader.hasNext()){
                String propName = jsonReader.nextName();


                switch (propName.toLowerCase()){
                    case "id":
                        movie.setId(jsonReader.nextInt());
                        break;
                    case "title":
                        movie.setTitle(jsonReader.nextString());
                        break;
                    case "production_year":
                        movie.setYear(jsonReader.nextInt());
                        break;
                    case "object_class":
                        isMovie = jsonReader.nextString().equalsIgnoreCase("Movie");
                        break;
                    case "fsk_level_list_facet":
                        jsonReader.beginArray();
                        while (jsonReader.hasNext()){
                            fskList.add(jsonReader.nextString().strip().toUpperCase());
                        }
                        jsonReader.endArray();
                        break;
                    default:
                        jsonReader.skipValue();
                        break;


                }

            }

            if (isMovie){
                addToMap(fskList, movie);
            }
            jsonReader.endObject();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close JSON Reader after Usage
     */
    public void close(){
        try {
            this.jsonReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add movie to the number of fsk found then add to map
     * @param fsks
     * @param movie
     */
    private void addToMap(List<String> fsks, Movie movie){
        fsks.forEach(fsk ->{
            AgeRating rating = this.resultMap.get(fsk);
            if (rating != null){
                rating.setCount(rating.getCount() + 1);
            }else {
                rating = new AgeRating();
                rating.setCount(1);
                rating.setFSK(fsk);
                this.resultMap.put(fsk, rating);
            }
            rating.addMovie(movie);

            System.out.println(String.format("Processed movie with id: %s", movie.getId()));
        });
    }

    /**
     * Get the result map
     * @return
     */
    public Map<String, AgeRating> getResultMap(){
        return this.resultMap;
    }

}
